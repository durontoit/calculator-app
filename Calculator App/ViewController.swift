//
//  ViewController.swift
//  Calculator App
//
//  Created by shahnazmul47@mgil.com on 7/6/21.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import RealmSwift
import CoreMotion
class ViewController: UIViewController {
    
    @IBOutlet var morningLabel: UILabel!
    @IBOutlet var greatDayLabel: UILabel!
    @IBOutlet var walkingView: UIView!
    @IBOutlet var walkingImageView: UIImageView!
    @IBOutlet var walkingLabel: UILabel!
    @IBOutlet var walkingStepCounterLabel: UILabel!
    @IBOutlet var walkingStepCounterNumberLabel: UILabel!
    @IBOutlet var sleepingView: UIView!
    @IBOutlet var sleepingImageView: UIImageView!
    @IBOutlet var sleepingLabel: UILabel!
    @IBOutlet var sleepingTimeLabel: UILabel!
    @IBOutlet var sleepingTimeNumberLabel: UILabel!
    @IBOutlet var heartView: UIView!
    @IBOutlet var heartImageView: UIImageView!
    @IBOutlet var heartLabel: UILabel!
    @IBOutlet var heartRateLabel: UILabel!
    @IBOutlet var heartRateNumberLabel: UILabel!
    @IBOutlet var traningView: UIView!
    @IBOutlet var traningImageView: UIImageView!
    @IBOutlet var traningLabel: UILabel!
    @IBOutlet var traningTimeLabel: UILabel!
    @IBOutlet var traningTimeNumberLabel: UILabel!
    var activityIndicatorView: NVActivityIndicatorView? = nil
    @IBOutlet var progressView: UIProgressView!
    var range = 100.0
    let activityManager = CMMotionActivityManager()
    let pedoMeter = CMPedometer()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initView()
        pedometerActivity()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    //MARK: - Color and initial value set
    
    func initView() {
        
        self.view.backgroundColor = UIColor(hex: whiteHexCode, alpha: 1.0)
        morningLabel.textColor = UIColor(hex: blackHexCode,alpha:1.0)
        morningLabel.font =  UIFont(name:fontBarlowSemiCondensedBold, size: CGFloat(fontSize20))
        morningLabel.text = "Good Morning, Yash"
        greatDayLabel.textColor = UIColor(hex: blackHexCode,alpha:1.0)
        greatDayLabel.font =  UIFont(name:fontBarlowSemiCondensedRegular, size: CGFloat(fontSize17))
        greatDayLabel.text = "Have a Great day Ahead!"
        walkingView.backgroundColor = UIColor.clear
        var gradientLayer = CAGradientLayer()
        gradientLayer.cornerRadius = 20.0
        gradientLayer.colors = [UIColor(hex: aliceBlueHexCode2,alpha:1.0).cgColor, UIColor(hex: aliceBlueHexCode1,alpha:1.0).cgColor]
           gradientLayer.locations = [0.0, 1.0]
           gradientLayer.frame = self.walkingView.bounds
        walkingView.layer.insertSublayer(gradientLayer, at:0)
        walkingImageView.image = UIImage(named: "outline_directions_walk_black_48pt")
        walkingLabel.textColor = UIColor(hex: blackHexCode,alpha:1.0)
        walkingLabel.font =  UIFont(name:fontPoppinsRegular, size: CGFloat(17))
        walkingLabel.text = "Walking"
        walkingStepCounterLabel.textColor = UIColor(hex: blackHexCode,alpha:1.0)
        walkingStepCounterLabel.font =  UIFont(name:fontPoppinsRegular, size: CGFloat(fontSize17))
        walkingStepCounterLabel.text = "Step Counter"
        walkingStepCounterNumberLabel.textColor = UIColor(hex: blackHexCode,alpha:1.0)
        walkingStepCounterNumberLabel.font =  UIFont(name:fontPoppinsBold, size: CGFloat(fontSize20))
        walkingStepCounterNumberLabel.text = ""
        progressView.progress = 0.0
        progressView.layer.cornerRadius = 10
        progressView.clipsToBounds = true
        progressView.layer.sublayers![1].cornerRadius = 10
        progressView.subviews[1].clipsToBounds = true
        progressView.tintColor = UIColor(hex: orangeHexColor, alpha: 1.0)
        progressView.progressTintColor = UIColor(hex: pinkHexCode2, alpha: 1.0)
        sleepingView.backgroundColor = UIColor.clear
         gradientLayer = CAGradientLayer()
        gradientLayer.cornerRadius = 20.0
        gradientLayer.colors = [UIColor(hex: nenoBlueHexCode2,alpha:1.0).cgColor, UIColor(hex: nenoBlueHexCode1, alpha:1.0).cgColor]
           gradientLayer.locations = [0.0, 1.0]
           gradientLayer.frame = self.sleepingView.bounds
        sleepingView.layer.insertSublayer(gradientLayer, at:0)
        sleepingImageView.image = UIImage(named: "outline_dark_mode_black_48pt")
        sleepingLabel.textColor = UIColor(hex: blackHexCode,alpha:1.0)
        sleepingLabel.font =  UIFont(name:fontPoppinsRegular, size: CGFloat(17))
        sleepingLabel.text = "Sleeping"
        sleepingTimeLabel.textColor = UIColor(hex: blackHexCode,alpha:1.0)
        sleepingTimeLabel.font =  UIFont(name:fontPoppinsRegular, size: CGFloat(fontSize17))
        sleepingTimeLabel.text = "Time"
        sleepingTimeNumberLabel.textColor = UIColor(hex: blackHexCode,alpha:1.0)
        sleepingTimeNumberLabel.font =  UIFont(name:fontPoppinsBold, size: CGFloat(fontSize20))
        sleepingTimeNumberLabel.text = ""
        heartView.backgroundColor = UIColor.clear
         gradientLayer = CAGradientLayer()
        gradientLayer.cornerRadius = 20.0
        gradientLayer.colors = [UIColor(hex: pinkHexCode2,alpha:1.0).cgColor, UIColor(hex: pinkHexCode1, alpha:1.0).cgColor]
           gradientLayer.locations = [0.0, 1.0]
           gradientLayer.frame = self.heartView.bounds
        heartView.layer.insertSublayer(gradientLayer, at:0)
        heartImageView.image = UIImage(named: "outline_favorite_black_48pt")
        heartLabel.textColor = UIColor(hex: blackHexCode,alpha:1.0)
        heartLabel.font =  UIFont(name:fontPoppinsRegular, size: CGFloat(17))
        heartLabel.text = "Heart"
        heartRateLabel.textColor = UIColor(hex: blackHexCode,alpha:1.0)
        heartRateLabel.font =  UIFont(name:fontPoppinsRegular, size: CGFloat(fontSize17))
        heartRateLabel.text = "Rate"
        heartRateNumberLabel.textColor = UIColor(hex: blackHexCode,alpha:1.0)
        heartRateNumberLabel.font =  UIFont(name:fontPoppinsBold, size: CGFloat(fontSize20))
        heartRateNumberLabel.text = ""
        traningView.backgroundColor = UIColor.clear
         gradientLayer = CAGradientLayer()
        gradientLayer.cornerRadius = 20.0
        gradientLayer.colors = [UIColor(hex: purpleHexCode2,alpha:1.0).cgColor, UIColor(hex: purpleHexCode1, alpha:1.0).cgColor]
           gradientLayer.locations = [0.0, 1.0]
           gradientLayer.frame = self.traningView.bounds
        traningView.layer.insertSublayer(gradientLayer, at:0)
        traningImageView.image = UIImage(named: "outline_schedule_black_48pt")
        traningLabel.textColor = UIColor(hex: blackHexCode,alpha:1.0)
        traningLabel.font =  UIFont(name:fontPoppinsRegular, size: CGFloat(17))
        traningLabel.text = "Traning"
        traningTimeLabel.textColor = UIColor(hex: blackHexCode,alpha:1.0)
        traningTimeLabel.font =  UIFont(name:fontPoppinsRegular, size: CGFloat(fontSize17))
        traningTimeLabel.text = "Time"
        traningTimeNumberLabel.textColor = UIColor(hex: blackHexCode,alpha:1.0)
        traningTimeNumberLabel.font =  UIFont(name:fontPoppinsBold, size: CGFloat(fontSize20))
        traningTimeNumberLabel.text = ""
        dataDownloadCheck()
        
    }
    
    //MARK: - pedoMeter
    func pedometerActivity(){
        
        self.activityManager.startActivityUpdates(to: OperationQueue.main) { (data) in
                        DispatchQueue.main.async {
                            if let activity = data {
                                if activity.running == true {
                                    print("Running")
                                }else if activity.walking == true {
                                    print("Walking")
                                }else if activity.automotive == true {
                                    print("Automative")
                                }
                            }
                        }
                    }
        
        self.pedoMeter.startUpdates(from: Date()) { (data, error) in
                        if error == nil {
                            if let response = data {
                                DispatchQueue.main.async { [self] in
                                    print("Number Of Steps == \(response.numberOfSteps)")

                                    self.walkingStepCounterNumberLabel.text = response.numberOfSteps.stringValue
                                    
                                    let step = Double(truncating: response.numberOfSteps) / range
                                    
                                    progressView.progress = Float(step)
                   
                                
                                }
                            }
                        }
                    }
    }
    
    
    //MARK: - create the alert
    func alert(message:String){
        
      
               let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)

               // add an action (button)
               alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

               // show the alert
               self.present(alert, animated: true, completion: nil)
   
    }
    //MARK: - Data Download
    
    func dataDownloadCheck(){
        
        if DBManager.sharedInstance.getServerData().count == 0{
            dataDownload()
    
        }else {
            let realm = try! Realm()
            let table = realm.objects(ServerData.self).filter("id == %@", 1).first!
            self.heartRateNumberLabel.text  = table.heart_rate
            self.sleepingTimeNumberLabel.text  = table.sleep_time
            self.traningTimeNumberLabel.text =  table.training_time
            
        }
        
    }
   
    func dataDownload(){
        
                if !Reachability.isConnectedToNetwork() {
                    DispatchQueue.main.async { [self] in
                        alert(message: "No data connection found. Please resubmit once you have data connection.")
                    }
                    return
                }
                
                
                let myView = UIView(frame:self.view.frame)
                myView.tag = 100
                myView.backgroundColor = UIColor(hex: blackHexCode,alpha: 0.1)
                let frame = CGRect(x: self.view.center.x-50, y: self.view.center.y-50, width: 100, height: 100)
                
                activityIndicatorView = NVActivityIndicatorView(frame: frame,
                                                                type: NVActivityIndicatorType.squareSpin)
                
                activityIndicatorView?.backgroundColor = UIColor.clear
                activityIndicatorView!.startAnimating()
                myView.addSubview(activityIndicatorView!)
                self.view.addSubview(myView)
                
        
        AF.request(url, method: .get, parameters: nil)
            .responseJSON { [self] (response) in
            
            let data = response.data
            if data != nil {
                
                do {
                    let json4Model = try JSONDecoder().decode(Json4Swift_Base.self, from:data!)
                    
                    let realm = try! Realm()
                    try!  realm.write {
                          let table = realm.objects(ServerData.self)
                        realm.delete(table)
                      }
                    
                    let table = ServerData()
                    table.id = 1
                    table.heart_rate = (json4Model.data?.heart_rate)!
                    table.sleep_time = (json4Model.data?.sleep_time)!
                    table.training_time = (json4Model.data?.training_time)!
             
                    try! realm.write {
                        realm.add(table)
                        
                    }
                        
                    self.heartRateNumberLabel.text  = table.heart_rate
                    self.sleepingTimeNumberLabel.text  = table.sleep_time
                    self.traningTimeNumberLabel.text =  table.training_time
                    
                    DispatchQueue.main.async { [self] in
                        if let viewWithTag = self.view.viewWithTag(100) {
                               viewWithTag.removeFromSuperview()
                           }
                        
                        self.activityIndicatorView!.stopAnimating()
                
                        
                        
                    }
                    
                } catch {
                    print(error)
                    DispatchQueue.main.async { [self] in
                        
                        if let viewWithTag = self.view.viewWithTag(100) {
                               viewWithTag.removeFromSuperview()
                           }
                    
                        
                        self.activityIndicatorView?.stopAnimating()
                        alert(message: "Internal Server Error")
                 
                    }
                }
                
            }

       }
        
    }


}

