//
//  DBManager.swift
//  Calculator App
//
//  Created by shahnazmul47@mgil.com on 7/6/21.
//

import UIKit
import RealmSwift

class DBManager {
    
    private var database:Realm
    static let sharedInstance = DBManager()
    
    private init() {
        database = try! Realm()
    }
    
    func isEmpty() -> Bool {
        return database.isEmpty
    }
    
    func getServerData() -> Results<ServerData> {
        
        let results = database.objects(ServerData.self)
        return results
        
        
    }
   
}
